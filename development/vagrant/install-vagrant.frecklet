doc:
  help: |
    Installs Vagrant (https://vagrantup.com) on a machine.

  short_help: installs Vagrant
  msg: installing Vagrant
force_update: false  # update Vagrant, even if it is already installed
vagrant_version: 2.1.2  # the version of Vagrant to install
vagrant_download_url: https://releases.hashicorp.com/vagrant  # the base download url
vagrant_download_path: "/tmp/_vagrant_download"  # the tempoary download folder
delete_download_after: yes  # whether to delete the downloaded install file after successful install

args:
  version:
    doc:
      help: the Version of Vagrant to install
    type: string
    required: false
    default: '2.1.2'
  update:
    doc:
      help: whether to set the newly installed Java to be the default on the system
    type: boolean
    default: false
    required: false
    cli:
      is_flag: true

tasks:
  - task:
      name: freckfrackery.install-vagrant
      type: ansible-role
      become: false
      needs_become: true
    vars:
      vagrant_version: "{{:: version ::}}"
      vagrant_force_update: "{{:: update ::}}"
